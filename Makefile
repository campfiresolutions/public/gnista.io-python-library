.DEFAULT_GOAL := help

# Show this help message
help:
	@cat $(MAKEFILE_LIST) | docker run --rm -i xanders/make-help

# prepare the build
prepare:
	pip install poetry
	poetry --version
	poetry config virtualenvs.in-project true
	poetry install -vv

build: prepare
	poetry build

# check styling
lint: prepare
	poetry run flake8 nista_library 
	poetry run pylint nista_library --rcfile=pylint.rc

# check types
types: prepare
	poetry run mypy nista_library

markdown:
	rm -rf Sphinx-docs/nista_library.rst
	rm -rf Sphinx-docs/_build
	poetry run sphinx-apidoc -o Sphinx-docs nista_library sphinx-apidoc --full -A 'Campfire Solutions GmbH';
	$(MAKE) -C Sphinx-docs markdown
	$(MAKE) -C Sphinx-docs latexpdf
	rm -rf api_doc
	mv Sphinx-docs/_build/markdown/README.md .
	cp -r Sphinx-docs/_build/markdown api_doc
	cp -r Sphinx-docs/_build/latex/nistaiopythonlibrary.pdf api_doc/

# run tests and generate report
test: prepare
	poetry run pytest tests/ --junitxml=report.xml

format: prepare
	poetry run black .

# run all build targets
all: format lint types test