# Examples

## Show received Data in a plot

The most easy way to receive data is to adresse the Datapoint directly and Plot it.
this is a snippet from the examples project

```shell
poetry new my-nista-client
cd my-nista-client
poetry add nista-library
poetry add structlog
poetry add matplotlib
poetry add tk
```

```default
from structlog import get_logger
from data_point_client.models.get_data_request import GetDataRequest

from nista_library import NistaConnection, NistaDataPoint

from plotter import Plotter

log = get_logger()


def direct_sample(connection: NistaConnection):
    data_point_id = "DATA_POINT_ID"
    data_point = NistaDataPoint(connection=connection, data_point_id=data_point_id)

    request = GetDataRequest(
        window_seconds=600,
        remove_time_zone=True,
    )

    data_point_data = data_point.get_data_point_data(request=request, timeout=90)

    log.info("Data has been received. Plotting")
    if isinstance(data_point_data, list):
        Plotter.plot(data_point_data)
```

## List DataPoints and filter by Name

You can list all DataPoints from your Workspace by querying the API. Use Filter lambda expressions in order to reduce the list to the entries you want.

In this example we use the Name in order to find DataPoints that start with “Chiller Cooling Power Production”

```default
import matplotlib.pyplot as plt
from structlog import get_logger
from data_point_client.models.get_data_request import GetDataRequest

from nista_library import NistaConnection, NistaDataPoints

from plotter import Plotter

log = get_logger()


def filter_by_name(connection: NistaConnection):
    dataPoints = NistaDataPoints(connection=connection)
    data_point_list = list(dataPoints.get_data_point_list())

    for data_point in data_point_list:
        log.info(data_point)

    # Find Specific Data Points
    filtered_data_points = filter(
        lambda data_point: data_point.name.startswith(
            "Chiller Cooling Power Production"
        ),
        data_point_list,
    )
    for data_point in filtered_data_points:
        request = GetDataRequest(
            window_seconds=600,
            remove_time_zone=True,
        )

        data_point_data = data_point.get_data_point_data(request=request, timeout=90)

        if isinstance(data_point_data, list):
            Plotter.plot(data_point_data)
```

## Filter by Physical Quantity

In order to find DataPoints by it’s Unit or Physical Quantity the filter query can be extended to load more data for every datapoint.

```default
from typing import List
from structlog import get_logger
from data_point_client.models.get_data_request import GetDataRequest

from nista_library import NistaConnection, NistaDataPoints, NistaDataPoint

from plotter import Plotter

log = get_logger()


def filter_by_unit(connection: NistaConnection):
    dataPoints = NistaDataPoints(connection=connection)
    data_point_list: List[NistaDataPoint] = list(dataPoints.get_data_point_list())

    for data_point in data_point_list:
        log.info(data_point)

    # Find Specific Data Points
    filtered_data_points = filter(
        lambda data_point: data_point.data_point_response.store.gnista_unit.physical_quantity.startswith(
            "Energy"
        ),
        data_point_list,
    )
    for data_point in filtered_data_points:
        log.info(data_point)
        request = GetDataRequest(
            window_seconds=600,
            remove_time_zone=True,
        )

        data_point_data = data_point.get_data_point_data(request=request, timeout=90)

        if isinstance(data_point_data, list):
            Plotter.plot(
                data_point_data, data_point.data_point_response.store.gnista_unit.name
            )
```
