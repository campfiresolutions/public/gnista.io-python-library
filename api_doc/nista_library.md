# nista_library package

## Submodules

## nista_library.nista_connetion module

Nista Connection

Contains all connections possible to nista.io


### _class_ nista_library.nista_connetion.KeyringNistaConnection(workspace_id, service_name='nista_library', base_url=None, enable_store_refresh_token=True, client_id=None, client_secret=None, verify_ssl=True)
Bases: `NistaConnection`

OAuth2.0 Connection which stores received access tokens in a local Keyring.


* **Enable_store_refresh_token**

    switch storage of refreshtoken on/off, in order to just keep access token for a limited period of time



* **Service_name**

    the keyring service name to be used to store access tokens



#### \__init__(workspace_id, service_name='nista_library', base_url=None, enable_store_refresh_token=True, client_id=None, client_secret=None, verify_ssl=True)

* **Parameters**

    
    * **workspace_id** (`str`) – The ID of the workspace to establish connection


    * **service_name** (`str`) – the keyring service name to be used to store access tokens, default is “nista_library”


    * **base_url** (`Optional`[`str`]) – “[https://app.nista.io](https://app.nista.io)” if left None


    * **enable_store_refresh_token** (`bool`) – switch storage of refreshtoken on/off, in order to just keep access token for a limited period of time


    * **client_id** (`Optional`[`str`]) – username for an optional login with OAuth2.0 Client/Secret Authentication


    * **client_secret** (`Optional`[`str`]) – secret for an optional login with OAuth2.0 Client/Secret Authentication


    * **verify_ssl** – define if the https connection will be checked for security



#### clear_stored_token()

### _class_ nista_library.nista_connetion.NistaConnection(workspace_id, base_url=None, datapoint_base_url=None, authentication_base_url=None, client_id=None, client_secret=None, verify_ssl=True)
Bases: `object`

Base class to open connection to nista.io
:scope: the list of scopes to be requested from the Authentication service


#### \__init__(workspace_id, base_url=None, datapoint_base_url=None, authentication_base_url=None, client_id=None, client_secret=None, verify_ssl=True)
Create NistaConnection


* **Parameters**

    
    * **workspace_id** (`str`) – The ID of the workspace to establish connection


    * **base_url** (`Optional`[`str`]) – “[https://app.nista.io](https://app.nista.io)” if left None


    * **datapoint_base_url** (`Optional`[`str`]) – “/api/datapoint” if left None


    * **authentication_base_url** (`Optional`[`str`]) – “/api/authentication” if left None


    * **client_id** (`Optional`[`str`]) – username for an optional login with OAuth2.0 Client/Secret Authentication


    * **client_secret** (`Optional`[`str`]) – secret for an optional login with OAuth2.0 Client/Secret Authentication


    * **verify_ssl** – define if the https connection will be checked for security



#### get_access_token()
starts authentication or uses refresh tokens to generate an OAuth2.0 Access Token, used for further interaction.


* **Raises**

    **ValueError** – If token can not be generated.



* **Return type**

    `str`



#### scope(_ = ['data-api'_ )

### _class_ nista_library.nista_connetion.StaticTokenNistaConnection(workspace_id, base_url=None, refresh_token=None, verify_ssl=True)
Bases: `NistaConnection`

Connection class that uses a static token string to establish connections


* **Refresh_token**

    the token to be used for connecting with nista.io



#### \__init__(workspace_id, base_url=None, refresh_token=None, verify_ssl=True)

* **Parameters**

    
    * **workspace_id** (`str`) – The ID of the workspace to establish connection


    * **base_url** (`Optional`[`str`]) – “[https://app.nista.io](https://app.nista.io)” if left None


    * **refresh_token** (`Optional`[`str`]) – the token to be used for connecting with nista.io


    * **verify_ssl** – define if the https connection will be checked for security


## nista_library.nista_credential_manager module


### _class_ nista_library.nista_credential_manager.NistaCredentialManager(service_information, proxies=None)
Bases: `CredentialManager`

Credential Manager to intercept and store OAuth Tokens.


#### \__init__(service_information, proxies=None)
## nista_library.nista_data_point module


### _class_ nista_library.nista_data_point.NistaDataPoint(connection, data_point_id, name=None)
Bases: `object`

Represents a DataPoint from nista.io
:DATE_FORMAT: Format to use for parse dictionaries
:DATE_NAME: Column Name for Dates
:VALUE_NAME: Column Name for Value


#### DATE_FORMAT(_ = '%Y-%m-%dT%H:%M:%SZ_ )

#### DATE_NAME(_ = 'Date_ )

#### VALUE_NAME(_ = 'Value_ )

#### \__init__(connection, data_point_id, name=None)
Load a DataPoint from nista.io


* **Parameters**

    
    * **connection** (`NistaConnection`) – To be used to connecto to nista.io


    * **data_point_id** (`UUID`) – The Unique ID to load the DataPoint


    * **name** (`Optional`[`str`]) – Optional Name to use for this DataPoint



#### append_data_point_data(data, unit=None, timeout=5.0)
Append data to an existing DataPoint


* **Parameters**

    
    * **data** (`Union`[`List`[`DataFrame`], `float`]) – To be added to a DataPoint


    * **unit** (`Optional`[`str`]) – The Unit to set on the DataPoint Store


    * **timeout** (`float`) – How long to wait for response



* **Return type**

    `Optional`[`Response`[`Union`[`Any`, `ProblemDetails`]]]



#### append_data_point_result_parts(data, unit, execution_id, timeout=5.0)
Append data as an execution Result. This method is used for nista.io Internal Execution handling


* **Parameters**

    
    * **data** (`Union`[`List`[`DataFrame`], `float`]) – To be added to a DataPoint


    * **unit** (`Optional`[`str`]) – The Unit to set on the DataPoint Store


    * **execution_id** (`UUID`) – the execution ID that this data is assignable to


    * **timeout** (`float`) – How long to wait for response



* **Return type**

    `Optional`[`Response`[`Union`[`Any`, `ProblemDetails`]]]



#### _classmethod_ create_new(connection, name, tags)
Creates a new DataPoint in nista.io


* **Parameters**

    
    * **connection** (`NistaConnection`) – To be used to connecto to nista.io


    * **name** (`str`) – The new name of the DataPoint


    * **tags** (`List`[`str`]) – List of Tags to add to the new DataPoint



* **Return type**

    `Optional`[`TypeVar`(`T`, bound= NistaDataPoint)]



* **Returns**

    The created DataPoint



#### _property_ data_point_response(_: DataPointResponseBase | Non_ )
Loads and interprets the DataPoint if it has not bee loaded.
:returns: The DataPoint Details from nista.io


#### get_data_point_data(request, post_fix=False, timeout=30)
Retrieves the Data from a DataPoint


* **Parameters**

    
    * **request** (`GetDataRequest`) – Request details for retrieving Data


    * **post_fix** (`bool`) – Append nista.io instance name after DataPoint Name


    * **timeout** (`float`) – How long to wait for response



* **Return type**

    `Union`[`List`[`DataFrame`], `float`, `Unset`, `WeekDataTransfere`, `DayDataByHourTransfer`, `None`]



* **Returns**

    The DataPoint Data



#### set_data_point_data(data, unit=None, execution_id=None, time_zone=None)
Replace or Set DataPoint Data with new Data. This bumps the DataPoint Version


* **Parameters**

    
    * **data** (`Union`[`List`[`DataFrame`], `float`, `WeekDataTransfere`]) – To be added to a DataPoint


    * **unit** (`Optional`[`str`]) – The Unit to set on the DataPoint Store


    * **execution_id** (`Optional`[`str`]) – the execution ID that this data is assignable to


    * **time_zone** (`Optional`[`ZoneInfo`]) – The Time Zone of the newly added Data



* **Return type**

    `Optional`[`Response`[`Union`[`Any`, `ProblemDetails`]]]


## nista_library.nista_data_points module


### _class_ nista_library.nista_data_points.NistaDataPoints(connection)
Bases: `object`

Represents a DataPoint List from nista.io


#### \__init__(connection)
Create a List of DataPoints


* **Parameters**

    **connection** (`NistaConnection`) – To be used to connecto to nista.io



#### get_data_point_list()
Retrieve a List of DataPoints for a nista.io workspace


* **Return type**

    `Generator`[`NistaDataPoint`, `None`, `None`]



* **Returns**

    List of DataPoints found in nista.io workspace


## Module contents

Nista Library

A client library for accessing nista.io
